package com.example.hmrequest.util;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class RequestUtil {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00202, "Network");

    public ZSONObject get(String requestUrl, Map<String, String> params) {
        HttpURLConnection connection = null;
        try {
            connection = getHttpURLConnection(appendParams(requestUrl, params),"GET");
            connection.connect();

            String response = RequestUtil.getReturnString(connection.getInputStream());
            ZSONObject zsonObject = ZSONObject.stringToZSON(response);
            HiLog.info(LABEL,"%{public}s", zsonObject);
            return zsonObject;

        } catch (Exception e) {
            HiLog.error(LABEL,"%{public}s", e.getMessage());
        }
        return null;
    }

     public ZSONObject post(String requestUrl, Map<String, String> params) {
        HttpURLConnection connection = null;
        try {
            connection = getHttpURLConnection(requestUrl,"POST");
            connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            connection.connect();

            String paramJson = ZSONObject.toZSONString(params);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
            writer.write(paramJson);
            writer.close();

            String response = RequestUtil.getReturnString(connection.getInputStream());
            ZSONObject zsonObject = ZSONObject.stringToZSON(response);
            HiLog.info(LABEL,"%{public}s", zsonObject);
            return zsonObject;

        } catch (Exception e) {
            HiLog.error(LABEL,"%{public}s", e.getMessage());
        }
        return null;
    }


    private HttpURLConnection getHttpURLConnection(String requestURL, String requestMethod) throws IOException {
        URL url = new URL(requestURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(10*1000);
        connection.setReadTimeout(15*1000);
        connection.setRequestMethod(requestMethod);
        return connection;
    }

    private static String getReturnString(InputStream is) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = "";

            while((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            String buf = sb.toString();
            return buf;
        } catch (Exception var5) {
            return null;
        }
    }

    private String appendParams(String path,Map<String, String> paramsMap) {
        if(paramsMap != null){
            path = path+"?";
            for (String key: paramsMap.keySet()){
                path = path + key+"="+paramsMap.get(key)+"&";
            }
            path = path.substring(0,path.length()-1);
        }
        return path;
    }
}
