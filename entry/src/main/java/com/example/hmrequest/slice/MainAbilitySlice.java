package com.example.hmrequest.slice;

import com.example.hmrequest.ResourceTable;
import com.example.hmrequest.util.RequestUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class MainAbilitySlice extends AbilitySlice {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    private Thread mThread;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
               // getRequest();
                postRequest();
            }
        });
        mThread.start();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void getRequest() {

        String requestUrl = "https://sapi.k780.com";
        HashMap params = new HashMap();
        ZSONObject zsonObject = new RequestUtil().get(requestUrl, null);
        HiLog.info(LABEL,"%{public}s", zsonObject);
    }

    private void postRequest() {

        String requestUrl = "https://blog.changshanhuoshan.cn/web/blog/list";
        HashMap params = new HashMap();
        params.put("pageNum", "1");
        params.put("pageSize", "10");
        ZSONObject zsonObject = new RequestUtil().post(requestUrl, params);
        HiLog.info(LABEL,"%{public}s", zsonObject);
    }


}
