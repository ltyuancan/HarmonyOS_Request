# HarmonyOS_Request

#### 介绍
鸿蒙网络请求简单封装和使用


#### 使用说明
要在子线程中使用网络请求：

```
 @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
               // getRequest();
                postRequest();
            }
        });
        mThread.start();
    }
```

1、GET请求

```
private void getRequest() {

    String requestUrl = "https://sapi.k780.com";
    HashMap params = new HashMap();
    ZSONObject zsonObject = new RequestUtil().get(requestUrl, null);
    HiLog.info(LABEL,"%{public}s", zsonObject);
}
```
2、POST请求

```
private void postRequest() {

    String requestUrl = "https://blog.changshanhuoshan.cn/web/blog/list";
    HashMap params = new HashMap();
    params.put("pageNum", "1");
    params.put("pageSize", "10");
    ZSONObject zsonObject = new RequestUtil().post(requestUrl, params);
    HiLog.info(LABEL,"%{public}s", zsonObject);
}

```
